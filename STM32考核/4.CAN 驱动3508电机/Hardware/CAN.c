#include "Serial.h"
#include "delay.h"
#include "led.h"
#include "stm32f10x.h"                  // Device header

CanRxMsg CAN_Rece_Data;
CanTxMsg CAN_Tran_Data;

u8 CAN_Mode_INIT(u32 tsjw, u32 tbs2, u32 tbs1, u16 brp, u32 mode)	
{

	CAN_InitTypeDef CAN_InitTypeStruct;
	
	CAN_InitTypeStruct.CAN_ABOM = DISABLE;
	CAN_InitTypeStruct.CAN_AWUM = DISABLE;
	CAN_InitTypeStruct.CAN_Mode = mode;     //模式设置
	CAN_InitTypeStruct.CAN_NART = ENABLE;   //错误重传
	CAN_InitTypeStruct.CAN_RFLM = DISABLE;
	CAN_InitTypeStruct.CAN_TTCM = DISABLE;  //非时间触发通信模式
	CAN_InitTypeStruct.CAN_TXFP = DISABLE;  //按ID优先级发送
	//配置成1Mbps
	
	
	
	
	
	CAN_InitTypeStruct.CAN_BS1 = tbs1;
	CAN_InitTypeStruct.CAN_BS2 = tbs2;
	CAN_InitTypeStruct.CAN_SJW = tsjw;
	CAN_InitTypeStruct.CAN_Prescaler = brp; //分频系数(Fdiv)为brp+1
	
	CAN_Init(CAN1,&CAN_InitTypeStruct);

	CAN_FilterInitTypeDef CAN_FilterInitTypeStruct;
	
	CAN_FilterInitTypeStruct.CAN_FilterActivation = ENABLE;
	CAN_FilterInitTypeStruct.CAN_FilterFIFOAssignment = CAN_Filter_FIFO0  ;
	CAN_FilterInitTypeStruct.CAN_FilterNumber = 0;
	
	CAN_FilterInitTypeStruct.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitTypeStruct.CAN_FilterMode = CAN_FilterMode_IdMask  ;
	
	CAN_FilterInitTypeStruct.CAN_FilterIdHigh = 0X0000;
	CAN_FilterInitTypeStruct.CAN_FilterIdLow = 0X0000;
	
	CAN_FilterInitTypeStruct.CAN_FilterMaskIdHigh = 0X0000;
	CAN_FilterInitTypeStruct.CAN_FilterMaskIdLow =0X0000;	

	CAN_FilterInit(&CAN_FilterInitTypeStruct);
	
	CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);
	
	return 0;
}


void CAN_GPIO_Config(void)
{
    GPIO_InitTypeDef GPIO_InitStructure;
	/* 使能CAN时钟 */
	RCC_APB1PeriphClockCmd (RCC_APB1Periph_CAN1 , ENABLE );
	/* 使能CAN引脚相关的时钟 */
 	RCC_APB2PeriphClockCmd ( RCC_APB2Periph_GPIOA | RCC_APB2Periph_AFIO, ENABLE );
	/* Configure CAN RX  pins */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IPU;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
	/* Configure CAN TX  pins */
    GPIO_InitStructure.GPIO_Pin = GPIO_Pin_12;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
    GPIO_Init(GPIOA, &GPIO_InitStructure);
	
}

void CAN_NVIC_Config(void)
{
	//CAN_ITConfig(CAN1,CAN_IT_FMP0,ENABLE);

	NVIC_InitTypeDef NVIC_InitStructure;
  
    /* 配置NVIC为优先级组1 */
    NVIC_PriorityGroupConfig(NVIC_PriorityGroup_1);
  
    NVIC_InitStructure.NVIC_IRQChannel = USB_LP_CAN1_RX0_IRQn;
    /* 配置抢占优先级 */
    NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 1;
    /* 配置子优先级 */
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
    /* 使能中断通道 */
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);
}

void CAN_INIT(void)
{
	CAN_GPIO_Config ();
	//CAN_Mode_INIT(CAN_SJW_2tq, CAN_BS2_4tq, CAN_BS1_4tq, 4, CAN_Mode_Normal); 
	CAN_Mode_INIT(CAN_SJW_2tq, CAN_BS2_3tq, CAN_BS1_5tq, 4, CAN_Mode_Normal); 
    CAN_NVIC_Config();
}



