#include "can.h"
#include "motor.h"

moto_measure_t moto_chassis[motor_num] = {0};		
moto_measure_t moto_info;

u8 get_moto_measure(moto_measure_t *ptr,CanRxMsg *CAN_Rece_Data)
{
	ptr->last_angle = ptr->angle;
	ptr->angle = (uint16_t)(CAN_Rece_Data->Data[0]<<8 | CAN_Rece_Data->Data[1]) ;
	ptr->real_current  = (int16_t)(CAN_Rece_Data->Data[2]<<8 | CAN_Rece_Data->Data[3]);
	ptr->speed_rpm = ptr->real_current;	//这里是因为两种电调对应位不一样的信息
	ptr->given_current = (int16_t)(CAN_Rece_Data->Data[4]<<8 | CAN_Rece_Data->Data[5])/-5;
	ptr->hall = CAN_Rece_Data->Data[6];
	if(ptr->angle - ptr->last_angle > 4096)
		ptr->round_cnt --;
	else if (ptr->angle - ptr->last_angle < -4096)
		ptr->round_cnt ++;
	ptr->total_angle = ptr->round_cnt * 8192 + ptr->angle - ptr->offset_angle;
	return CAN_Rece_Data->DLC;
}

u8 set_moto_current(CanTxMsg* CAN_Tran_Data, s16 SID, s16 iq1)
{    
	//uint8_t box;
	
	CAN_Tran_Data->StdId = SID;
	CAN_Tran_Data->IDE = CAN_ID_STD;
	CAN_Tran_Data->RTR = CAN_RTR_DATA;
	CAN_Tran_Data->DLC = 0x08;
	CAN_Tran_Data->Data[0] = iq1 >> 8;
	CAN_Tran_Data->Data[1] = iq1;

	
	//box = CAN_Transmit(CAN1,CAN_Tran_Data);
	CAN_Transmit(CAN1,CAN_Tran_Data);
	//while(CAN_TransmitStatus(CAN1,box) == CAN_TxStatus_Failed){};
	
	
    //return 0;	
}	






