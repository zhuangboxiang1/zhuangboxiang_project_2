#ifndef __PWM_H
#define __PWM_H

void PWM_Init(void);
void PWM_SetCompare1(unsigned int Compare);
void PWM_SetPrescaler(unsigned int Prescaler);

#endif


