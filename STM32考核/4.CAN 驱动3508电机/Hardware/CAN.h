#ifndef __CAN_H
#define __CAN_H
#include "mytype.h"

//CAN1接收RX0中断使能
//#define CAN1_RX0_INT_ENABLE	1		//0,不使能;1,使能.


void CAN_INIT(void);
extern CanRxMsg CAN_Rece_Data;
extern CanTxMsg CAN_Tran_Data;

#endif


