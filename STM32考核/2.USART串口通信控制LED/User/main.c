#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "Serial.h"
#include "Key.h"
#include <string.h>
#include "LED.h"

int main(void)
{

	LED_Init();

	Serial_Init();
	

	

	
	while (1)
	{
		if(Serial_RxFlag == 1)
		{

			
			if(strcmp(Serial_RxPacket,"LED_ON") == 0)
			{
				LED1_ON();
				Serial_SendString("LED_ON_OK\r\n");

			}
			else if(strcmp(Serial_RxPacket,"LED_OFF") == 0)
			{
				LED1_OFF();
				Serial_SendString("LED_OFF_OK\r\n");
            }
			else 
			{
				Serial_SendString("ERROR_COMMAND\r\n");

			}
			Serial_RxFlag = 0;
		}
	}
}
