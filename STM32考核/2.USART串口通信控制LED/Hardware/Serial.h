#ifndef __SERIAL_H
#define __SERIAL_H

#include <stdio.h>

extern char Serial_RxPacket[];
extern unsigned char Serial_RxFlag;

void Serial_Init(void);
void Serial_SendByte(unsigned char Byte);
void Serial_SendArray(unsigned char *Array,unsigned int Length);
void Serial_SendString(char *String);






#endif
