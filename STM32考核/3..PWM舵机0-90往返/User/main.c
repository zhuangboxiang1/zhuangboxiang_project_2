#include "stm32f10x.h"                  // Device header
#include "Delay.h"
#include "OLED.h"
#include "PWM.h"
#include "Key.h"

int main(void)
{
	PWM_Init();
	Key_Init();
	

	while (1)
	{
		int Angle;
		int key = Key_GetNum();
		if(key ==  1)
		{
			Angle = 1500;
		}
		else
		{
			Angle = 500;
		}
		PWM_SetCompare2(Angle);
	}
}

