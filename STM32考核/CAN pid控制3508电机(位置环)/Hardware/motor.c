#include "can.h"
#include "motor.h"

moto_measure_t moto_chassis[motor_num] = {0};		//4 chassis moto
moto_measure_t moto_info;


u8 get_moto_measure(moto_measure_t *ptr,CanRxMsg *CAN_Rece_Data)
{
	ptr->last_angle = ptr->angle;
	ptr->angle = (uint16_t)(CAN_Rece_Data->Data[0]<<8 | CAN_Rece_Data->Data[1]) ;
	ptr->real_current  = (int16_t)(CAN_Rece_Data->Data[2]<<8 | CAN_Rece_Data->Data[3]);
	ptr->speed_rpm = ptr->real_current;	//这里是因为两种电调对应位不一样的信息
	ptr->given_current = (int16_t)(CAN_Rece_Data->Data[4]<<8 | CAN_Rece_Data->Data[5])/-5;
	ptr->hall = CAN_Rece_Data->Data[6];
	if(ptr->angle - ptr->last_angle > 4096)
		ptr->round_cnt --;
	else if (ptr->angle - ptr->last_angle < -4096)
		ptr->round_cnt ++;
	ptr->total_angle = ptr->round_cnt * 8192 + ptr->angle - ptr->offset_angle;
	return CAN_Rece_Data->DLC;
}

//u8 set_moto_current(CanTxMsg* CAN_Tran_Data, s16 SID, s16 iq1)
//{    
//	uint8_t box;
//	
//	CAN_Tran_Data->StdId = SID;
//	CAN_Tran_Data->IDE = CAN_ID_STD;
//	CAN_Tran_Data->RTR = CAN_RTR_DATA;
//	CAN_Tran_Data->DLC = 0x08;
//	CAN_Tran_Data->Data[0] = (int8_t)(iq1 >> 8);
//	CAN_Tran_Data->Data[1] = (int8_t)(iq1&0x00ff);

//	
//	box = CAN_Transmit(CAN1,CAN_Tran_Data);
//			
//	while(CAN_TransmitStatus(CAN1,box) == CAN_TxStatus_Failed){};
//	
//	
//    return 0;	
//}	


//CAN中断处理过程
//此函数会被CAN_Receive_IT()调用
//hcan:CAN句柄
//void HAL_CAN_RxCpltCallback(CAN_HandleTypeDef *hcan)
//{
//	if (hcan->Instance == CAN1)
//	{
//		//CAN_Receive_IT()函数会关闭FIFO0消息挂号中断，因此我们需要重新打开
//		__HAL_CAN_ENABLE_IT(&CAN1_Handler, CAN_IT_FMP0); //重新开启FIF00消息挂号中断
//		i = hcan->pRxMsg->StdId - 0x201;
//		switch(hcan->pRxMsg->StdId){
//			case 0x201: 
//			case 0x202: 
//			case 0x203: 
//			case 0x204: 
//				if (moto_chassis[i].msg_cnt++ <= 50)
//				    get_moto_offset(&moto_chassis[i], hcan);
//				else get_moto_measure(&moto_chassis[i], hcan);
//				break;
//			default:break;
//		}
//	}
//}



