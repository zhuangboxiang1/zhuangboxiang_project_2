///*#include "motor_pid.h"
//#include "mytype.h"
//#include <math.h>

//#define ABS(x)		((x>0)? (x): (-x)) 

//void abs_limit(float *a, float ABS_MAX){
//    if(*a > ABS_MAX)
//        *a = ABS_MAX;
//    if(*a < -ABS_MAX)
//        *a = -ABS_MAX;
//}
///*参数初始化--------------------------------------------------------------*/
//static void pid_param_init(
//    pid_t *pid, 
//    uint32_t mode,
//    uint32_t maxout,
//    uint32_t intergral_limit,
//    float 	kp, 
//    float 	ki, 
//    float 	kd)
//{
//    pid->IntegralLimit = intergral_limit;
//    pid->MaxOutput = maxout;
//    pid->pid_mode = mode;
//    pid->p = kp;
//    pid->i = ki;
//    pid->d = kd;
//}
///*中途更改参数设定(调试)------------------------------------------------------------*/
//static void pid_reset(pid_t	*pid, float kp, float ki, float kd)
//{
//    pid->p = kp;
//    pid->i = ki;
//    pid->d = kd;
//}

////float pid_calc1(pid_t* pid, float get, float set)
////{
////	pid->err[NOW] = get -set;
////	pid->pout +=  pid->err[NOW];
////	pid->pos_out = pid->p*pid->pout + pid->i*pid->iout;
////	
////	return 0;
////}



//float pid_calc(pid_t* pid, float get, float set){
//    pid->get[NOW] = get;
//    pid->set[NOW] = set;
//    pid->err[NOW] = set - get;	//set - measure
//    if (pid->max_err != 0 && ABS(pid->err[NOW]) >  pid->max_err  )
//		return 0;
//	if (pid->deadband != 0 && ABS(pid->err[NOW]) < pid->deadband)
//		return 0;
//    
//    if(pid->pid_mode == POSITION_PID) //位置式p
//    {
//			
//        pid->pout = pid->p * pid->err[NOW];
//        pid->iout += pid->i * pid->err[NOW];
//        pid->dout = pid->d * (pid->err[NOW] - pid->err[LAST] );
//        abs_limit(&(pid->iout), pid->IntegralLimit);
//        pid->pos_out = pid->pout + pid->iout + pid->dout;
//        abs_limit(&(pid->pos_out), pid->MaxOutput);
//        pid->last_pos_out = pid->pos_out;	//update last time 
//    }
//    pid->err[LLAST] = pid->err[LAST];
//    pid->err[LAST] = pid->err[NOW];
//    pid->get[LLAST] = pid->get[LAST];
//    pid->get[LAST] = pid->get[NOW];
//    pid->set[LLAST] = pid->set[LAST];
//    pid->set[LAST] = pid->set[NOW];
//    return pid->pid_mode==POSITION_PID ? pid->pos_out : pid->delta_out;
////	
//}


////float pid_calc2(pid_t *pid,float get,float set)
////{
////	pid->get[NOW] = get;
////	pid->set[NOW] = set;
////	pid->err[NOW] = set - get;
////	
////  #if (PID_MODE == POSITION_PID)
////      pid->pout = pid->p * pid->error[NOW_ERR];
////      pid->iout += pid->i * pid->error[NOW_ERR];
////      pid->dout = pid->d * (pid->error[NOW_ERR] - pid->error[LAST_ERR]);
////      
////      abs_limit(&(pid->iout),pid->integral_limit);
////      pid->out = pid->pout + pid->iout + pid->dout;
////      abs_limit(&(pid->out),pid->maxout);
////  #elif (PID_MODE == DELTA_PID)
////      pid->pout = pid->kp * (pid->error[NOW_ERR] - pid->error[LAST_ERR]);
////      pid->iout = pid->ki * pid->error[NOW_ERR];
////      pid->dout = pid->kd * (pid->error[NOW_ERR] * pid->error[LAST_ERR] + pid->error[LLAST_ERR]);

////      pid->out += pid->pout + pid->iout + pid->dout;
////      abs_limit(&(pid->out), pid->maxout);
////	#endif
////  
////  pid->error[LLAST_ERR] = pid->error[LAST_ERR];
////  pid->error[LAST_ERR]  = pid->error[NOW_ERR];
////  
////  if ((pid->output_deadband != 0) && (fabs(pid->out) < pid->output_deadband))
////    return 0;
////  else
////    return pid->out;
////  
////}	






///*pid总体初始化-----------------------------------------------------------------*/
//void PID_struct_init(
//    pid_t* pid,
//    uint32_t mode,
//    uint32_t maxout,
//    uint32_t intergral_limit,
//    
//    float 	kp, 
//    float 	ki, 
//    float 	kd)
//{
//    /*init function pointer*/
//    pid->f_param_init = pid_param_init;
//    pid->f_pid_reset = pid_reset;
////	pid->f_cal_pid = pid_calc;	
////	pid->f_cal_sp_pid = pid_sp_calc;	//addition
//		
//    /*init pid param */
//    pid->f_param_init(pid, mode, maxout, intergral_limit, kp, ki, kd);
//}


