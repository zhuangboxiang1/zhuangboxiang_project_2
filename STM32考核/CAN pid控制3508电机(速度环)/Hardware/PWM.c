#include "stm32f10x.h"                  // Device header

void PWM_Config(u16 arr, u16 psc)
{
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA, ENABLE);
	
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_AFIO, ENABLE);
	GPIO_PinRemapConfig(GPIO_PartialRemap_TIM3, ENABLE);
	GPIO_PinRemapConfig(GPIO_Remap_SWJ_JTAGDisable, ENABLE);
	
	GPIO_InitTypeDef GPIO_InitStructure;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin =GPIO_Pin_7 |GPIO_Pin_8 |GPIO_Pin_9;		
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOC, &GPIO_InitStructure);
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF_PP;
	GPIO_InitStructure.GPIO_Pin =GPIO_Pin_6;		
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_Init(GPIOA, &GPIO_InitStructure);
	
	TIM_InternalClockConfig(TIM3);
	
	TIM_TimeBaseInitTypeDef TIM_TimeBaseInitStructure;
	TIM_TimeBaseInitStructure.TIM_ClockDivision = TIM_CKD_DIV1;
	TIM_TimeBaseInitStructure.TIM_CounterMode = TIM_CounterMode_Up;
	TIM_TimeBaseInitStructure.TIM_Period = arr;		//ARR
	TIM_TimeBaseInitStructure.TIM_Prescaler = psc;		//PSC
	TIM_TimeBaseInitStructure.TIM_RepetitionCounter = 0;
	TIM_TimeBaseInit(TIM3, &TIM_TimeBaseInitStructure);
	
	
	
	TIM_OCInitTypeDef TIM_OCInitStructure_1;
	TIM_OCStructInit(&TIM_OCInitStructure_1);
	TIM_OCInitStructure_1.TIM_OCMode = TIM_OCMode_PWM1;
	TIM_OCInitStructure_1.TIM_OCPolarity = TIM_OCPolarity_High;
	TIM_OCInitStructure_1.TIM_OutputState = TIM_OutputState_Enable;
	TIM_OCInitStructure_1.TIM_Pulse = arr/2;		//CCR
	TIM_CCxCmd(TIM3,TIM_Channel_1,TIM_CCx_Enable);
	TIM_OC1Init(TIM3, &TIM_OCInitStructure_1);
	
//	TIM_OCInitTypeDef TIM_OCInitStructure_2;
//	TIM_OCStructInit(&TIM_OCInitStructure_2);
//	TIM_OCInitStructure_2.TIM_OCMode = TIM_OCMode_PWM1;
//	TIM_OCInitStructure_2.TIM_OCPolarity = TIM_OCPolarity_High;
//	TIM_OCInitStructure_2.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure_2.TIM_Pulse = arr/2;		//CCR
//	TIM_CCxCmd(TIM3,TIM_Channel_2,TIM_CCx_Enable);
//	TIM_OC1Init(TIM3, &TIM_OCInitStructure_2);
//	
//	TIM_OCInitTypeDef TIM_OCInitStructure_3;
//	TIM_OCStructInit(&TIM_OCInitStructure_3);
//	TIM_OCInitStructure_3.TIM_OCMode = TIM_OCMode_PWM1;
//	TIM_OCInitStructure_3.TIM_OCPolarity = TIM_OCPolarity_High;
//	TIM_OCInitStructure_3.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure_3.TIM_Pulse = arr/2;		//CCR
//	TIM_CCxCmd(TIM3,TIM_Channel_3,TIM_CCx_Enable);
//	TIM_OC1Init(TIM3, &TIM_OCInitStructure_3);
//	
//	TIM_OCInitTypeDef TIM_OCInitStructure_4;
//	TIM_OCStructInit(&TIM_OCInitStructure_4);
//	TIM_OCInitStructure_4.TIM_OCMode = TIM_OCMode_PWM1;
//	TIM_OCInitStructure_4.TIM_OCPolarity = TIM_OCPolarity_High;
//	TIM_OCInitStructure_4.TIM_OutputState = TIM_OutputState_Enable;
//	TIM_OCInitStructure_4.TIM_Pulse = arr/2;		//CCR
//	TIM_CCxCmd(TIM3,TIM_Channel_4,TIM_CCx_Enable);
//	TIM_OC1Init(TIM3, &TIM_OCInitStructure_4);
	
	TIM_Cmd(TIM3, ENABLE);
}

void PWM_Init(void)
{
        PWM_Config(20000 - 1, 108 - 1); //108M/108=1M的计数频率，自动重装载为20000，那么PWM频率为1M/2000=50HZ

        TIM_SetCompare1(TIM3,1500);
//		TIM_SetCompare2(TIM3,1500);
//		TIM_SetCompare3(TIM3,1500);
//		TIM_SetCompare4(TIM3,1500);
}



//void PWM_SetCompare1(uint16_t Compare)
//{
//	TIM_SetCompare1(TIM2, Compare);
//}

//void PWM_SetPrescaler(uint16_t Prescaler)
//{
//	TIM_PrescalerConfig(TIM2, Prescaler, TIM_PSCReloadMode_Immediate);
//}


