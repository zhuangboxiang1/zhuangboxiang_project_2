#ifndef __MOTOR
#define __MOTOR

#include "stm32f10x.h"                  // Device header


#include "mytype.h"
#include "can.h"


/*CAN发送或是接收的ID*/
typedef enum
{

	CAN_TxPY12V_ID 	= 0x200,		//云台12V发送ID
	CAN_TxPY24V_ID	= 0x1FF,		//云台12V发送ID
//	CAN_Pitch_ID 	= 0x201,			//云台Pitch
//	CAN_Yaw_ID   	= 0x203,			//云台Yaw
	CAN_YAW_FEEDBACK_ID   = 0x205,		//云台Yaw24v
	CAN_PIT_FEEDBACK_ID  = 0x206,			//云台Yaw24v
	CAN_POKE_FEEDBACK_ID  = 0x207,
	CAN_ZGYRO_RST_ID 			= 0x404,
	CAN_ZGYRO_FEEDBACK_MSG_ID = 0x401,
	CAN_MotorLF_ID 	= 0x041,    //左前
	CAN_MotorRF_ID 	= 0x042,		//右前
	CAN_MotorLB_ID 	= 0x043,    //左后
	CAN_MotorRB_ID 	= 0x044,		//右后

	CAN_EC60_four_ID	= 0x200,	//EC60接收程序
	CAN_backLeft_EC60_ID = 0x203, //ec60
	CAN_frontLeft_EC60_ID = 0x201, //ec60
	CAN_backRight_EC60_ID = 0x202, //ec60
	CAN_frontRight_EC60_ID = 0x204, //ec60
	
	//add by langgo
	CAN_3510Moto_ALL_ID = 0x200,
	CAN_3510Moto1_ID = 0x201,
	CAN_3510Moto2_ID = 0x202,
	CAN_3510Moto3_ID = 0x203,
	CAN_3510Moto4_ID = 0x204,
	CAN_DriverPower_ID = 0x80,
	
	CAN_HeartBeat_ID = 0x156,
	
}CAN_Message_ID;

#define FILTER_BUF_LEN		5
/*电机的参数结构体*/
typedef struct{
	int16_t	 	speed_rpm;                   // 电机转速，单位为每分钟
    int16_t  	real_current;                //真实电流
    int16_t  	given_current;               //给定电流
    uint8_t  	hall;                        // 传感器检测到的霍尔信号
	uint16_t 	angle;				         // 绝对角度范围为[0,8191]
	uint16_t 	last_angle;	                 // 上一次的绝对角度范围为[0,8191]
	uint16_t	offset_angle;                //偏移角度
	int32_t		round_cnt;                   //转动次数
	int32_t		total_angle;                 //总角度
	u8			buf_idx;                     // 缓冲区索引
	u16			angle_buf[FILTER_BUF_LEN];   // 角度缓冲区
	u16			fited_angle;                 // 适配后的角度
	u32			msg_cnt;                     // 消息计数
}moto_measure_t;


// CAN总线电机的信息，主要关注转速和电流值
typedef struct{
	moto_measure_t motor1;
	moto_measure_t motor2;
	moto_measure_t motor3;
}CAN_MotoInfo_t;


#define motor_num 1		//电机数量
#define CAN_CONTROL		// CAN总线控制
// #define PWM_CONTROL		// PWM控制

/* Extern  ------------------------------------------------------------------*/
extern moto_measure_t  moto_chassis[];
extern moto_measure_t moto_info;
extern CAN_MotoInfo_t CAN_MotoInfo;

u8 get_moto_measure(moto_measure_t *ptr,CanRxMsg *CAN_Rece_Data);
//u8 set_moto_current(CanTxMsg* CAN_Tran_Data, s16 SID, s16 iq1);
#endif


